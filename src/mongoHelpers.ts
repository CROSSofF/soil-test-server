import { Request, Response } from "express";
import { Collection } from "mongodb";
import { Spectrum } from "./model/Spectrum";

export const getSpectrumById = (spectrumsCollection: Collection<Spectrum>, req: Request, res: Response) => {
    spectrumsCollection.findOne({ _id: req.params.id})
    .then((value) => {
        if (value) {
            res.send(value);
        } else {
            res.status(404);
            res.send();
        }
    })
    .catch((reason) => {
        console.log("request: ", req);
        console.log(reason);
        res.statusCode = 400;
        res.send();
    });
};

export const getRandomSpectrum = (spectrumsCollection: Collection<Spectrum>, req: any, res: any) => {
    spectrumsCollection.findOne({})
    .then((value) => {
        console.log(value);
        res.send(value);
    })
    .catch((reason) => {
        console.log("request: ", req);
        console.log(reason);
        res.statusCode = 500;
        res.send();
    });
}