import { v4 } from "uuid";

export class Sample {
    private static idCounter = 0;
    private static date = new Date();

    public _id: Readonly<string>;
    public traceesId: Readonly<string>;

    constructor() {
        this._id = v4();
        this.traceesId = this.generateTRACEESId();
    }

    private generateTRACEESId(): string {
        Sample.idCounter++;
        if (Sample.idCounter > 999) {
            Sample.idCounter -= 999;
            Sample.date.setDate(Sample.date.getDate() + 1);
        }

        const dateString = `${Sample.date.getFullYear()}-${Sample.date.getMonth()}-${Sample.date.getDate()}`;
        const idString = Sample.idCounter.toString().padStart(3, "0");

        return `TEST-TRACEES-${dateString}-${idString}`;
    }
}