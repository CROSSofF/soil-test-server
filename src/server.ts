import express, { Request, Response } from "express";
import fs from "fs";

import { MongoClient, Db, Collection, MongoClientOptions } from "mongodb";
import { Spectrum } from "./model/Spectrum";

import { getRandomSpectrum, getSpectrumById } from "./mongoHelpers";

let duneTestDB: Db;
let spectrumsCollection: Collection<Spectrum>;

const wavenumberFile = fs.readFileSync("./res/wavenumbers.json");
const wavenumberData = JSON.parse(wavenumberFile as any);

async function initMongoClient() {
    const mongoConfig = JSON.parse(fs.readFileSync("./res/mongoConfig.json") as any);
    const username = encodeURIComponent(mongoConfig.username);
    const password = encodeURIComponent(mongoConfig.password);

    const clusterURL = encodeURIComponent("mongodb.unimelb-tracees-ftir.cloud.edu.au");

    const options: MongoClientOptions = {
        authMechanism: "DEFAULT",
        tls: true,
        tlsCertificateKeyFile: "./cert/ftir_client_0.keypair.pem",
        tlsCAFile: "./cert/ftir_ca.cer",
        tlsCertificateKeyFilePassword: mongoConfig.pemPassword,
        tlsAllowInvalidHostnames: true, 
    };

    try {
        const client = new MongoClient(`mongodb://${username}:${password}@${clusterURL}`, options);
        await client.connect();

        duneTestDB = client.db("dune-test");
        spectrumsCollection = duneTestDB.collection<Spectrum>("spectrums");
    } catch (err) {
        console.error(err);
        process.exit(-1);
    } finally {
        console.log("# Successfully initialized MongoDB client");
    }
}

async function initExpress() {
    const app = express();
    const port = 9443; // default port to listen

    app.use(express.static("./build"))
    app.get(`/spectrum/:id`, getSpectrumById.bind(spectrumsCollection));
    app.get(`/data`, getRandomSpectrum.bind(spectrumsCollection));
    app.get(`/wavenumbers`, (_: Request, res: Response) => res.send(wavenumberData));

    app.listen(port, () => {
        console.log(`Started server on http://localhost:9443`);
    });
}

initMongoClient()
.then(initExpress)
.catch((reason) => {
    console.error(reason);
});
