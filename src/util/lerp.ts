export function lerp(x: number, start: number, end: number) {
    return start + (x*(end - start))
}