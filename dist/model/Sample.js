"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Sample = void 0;
var uuid_1 = require("uuid");
var Sample = /** @class */ (function () {
    function Sample() {
        this._id = uuid_1.v4();
        this.traceesId = this.generateTRACEESId();
    }
    Sample.prototype.generateTRACEESId = function () {
        Sample.idCounter++;
        if (Sample.idCounter > 999) {
            Sample.idCounter -= 999;
            Sample.date.setDate(Sample.date.getDate() + 1);
        }
        var dateString = Sample.date.getFullYear() + "-" + Sample.date.getMonth() + "-" + Sample.date.getDate();
        var idString = Sample.idCounter.toString().padStart(3, "0");
        return "TEST-TRACEES-" + dateString + "-" + idString;
    };
    Sample.idCounter = 0;
    Sample.date = new Date();
    return Sample;
}());
exports.Sample = Sample;
