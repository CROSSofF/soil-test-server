"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getRandomSpectrum = exports.getSpectrumById = void 0;
var getSpectrumById = function (spectrumsCollection, req, res) {
    spectrumsCollection.findOne({ _id: req.params.id })
        .then(function (value) {
        if (value) {
            res.send(value);
        }
        else {
            res.status(404);
            res.send();
        }
    })
        .catch(function (reason) {
        console.log("request: ", req);
        console.log(reason);
        res.statusCode = 400;
        res.send();
    });
};
exports.getSpectrumById = getSpectrumById;
var getRandomSpectrum = function (spectrumsCollection, req, res) {
    spectrumsCollection.findOne({})
        .then(function (value) {
        console.log(value);
        res.send(value);
    })
        .catch(function (reason) {
        console.log("request: ", req);
        console.log(reason);
        res.statusCode = 500;
        res.send();
    });
};
exports.getRandomSpectrum = getRandomSpectrum;
